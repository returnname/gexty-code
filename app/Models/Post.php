<?php

namespace App\Models;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = ['title' , 'text' , 'user_id'];

    protected $appends = ['preview'];


    public function getPreviewAttribute(){
        return str_limit($this->text , 100);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }




}