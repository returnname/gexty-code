<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AuthenticateJWTToken extends BaseMiddleware
{

    public function handle(Request $request, Closure $next)
    {

        $this->authenticate($request);

        return $next($request);
    }

}