<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Http\Resources\Posts as PostCollection;
use App\Http\Controllers\Controller;
use App\Http\Resources\Post as PostResource;

class PostController extends Controller
{
    public function index(Post $post){
        return (new PostCollection($post->with('user')->paginate(50)));
    }

    public function show(Post $post){
        return (new PostResource($post));
    }


}