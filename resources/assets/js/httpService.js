import axios from 'axios';
import tokenService from './services/jwtTokenService'

const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

let headers = { 'X-CSRF-TOKEN': csrfToken };

if (tokenService.hasToken()) headers.Authorization = 'Bearer ' + tokenService.getToken();

const httpService = axios.create({ headers });

export  default  httpService;