import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom'

import MainPage from '../containers/MainPageContainer';
import PostPage from '../containers/PostPageContainer';
import LoginPage from "../containers/LoginPageContainer";


export default class Content extends Component {
    render() {
        return (
            <div className="c-content">
                <div className="c-content__container">
                    <Switch>
                        <Route exact path='/' component={MainPage}/>
                        <Route path='/login' component={LoginPage}/>
                        <Route path='/post/:id' component={PostPage}/>
                    </Switch>
                </div>
            </div>
        );
    }
}


