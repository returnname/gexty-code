import React, {Component} from 'react';
import FeedItem from './FeedItem';

import throttler from '../../utilities/throttler';

class FeedList extends Component {
    constructor(props){
        super(props);
        this.handleOnScrollWithThrottle = throttler(this.handleOnScroll).bind(this)
    }

    componentDidMount(){
        this.fetchPostsData();
        window.addEventListener('scroll', this.handleOnScrollWithThrottle);
    }

    componentWillUnmount(){
        window.removeEventListener('scroll', this.handleOnScrollWithThrottle);
    }

    fetchPostsData = () =>{
        const{ postActions , page , lastPage } = this.props;
        if (page && (page == lastPage)) return;
        postActions.fetchPostsFromApi(page);
    }

    handleOnScroll = () => {
        const { innerHeight, scrollY } = window;
        const { body: { offsetHeight } } = document;

        if ((innerHeight + scrollY) >= offsetHeight)
            this.fetchPostsData()
    }

    render() {
        const { posts , isFetchingData , page, lastPage } = this.props;

        return (
            <div className="feed-container">
                {posts && posts.map((item, key) => <FeedItem key={key} {...item}/>)}

                {isFetchingData &&
                    <div className="feed-loader">
                        <i className="fas fa-spinner feed-loader__spinner"/>
                    </div>
                }

                {(page < lastPage) &&
                    <div className="feed-more-items" onClick={this.fetchPostsData}>
                        <a className="feed-more-items__button">Загрузить еще...</a>
                    </div>
                }
            </div>
        );
    }
}

export default FeedList;
