import React from 'react';
import { Link } from 'react-router-dom';
import Greet from '../Greet'


const FeedItem = (props) => {
    return(
        <div className="feed-item">
            <div className="feed-item__header">
                <img src="/images/no-image.jpg" className="feed-item__header__logo"/>
                <a className="feed-item__header__author">
                    {props.user.name}
                </a>
                <div className="feed-item__header__greet">
                    <Greet active={true} count={99}></Greet>
                </div>
            </div>
            <div className="feed-item__title">
                <Link to={`/post/${props.id}`} className="feed-item__title__link">
                    {props.title}
                </Link>
            </div>
            <p className="feed-item__preview">
                {props.preview}
            </p>
            <div className="feed-item__thumb">
                <img src="/images/thumb.jpeg" className="feed-item__thumb__image"/>
            </div>
            <div className="feed-item__tags">
                <span className="feed-item__tags__item">Linux</span>
            </div>
        </div>
    );
};

export default FeedItem;


