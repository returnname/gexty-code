import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {
    render() {
        return (
            <div className="c-header">
                <div className="c-header__container">
                    <div className="header-logo">
                        <Link to="/" className="header-logo__text">
                            GextyCode
                        </Link>
                    </div>
                    <ul className="header-menu">
                        <li className="header-menu__item header-menu__item-active">
                            <a href="asd" className="header-menu__item__link">Новое</a>
                        </li>
                        <li className="header-menu__item"><a href="asd" className="header-menu__item__link">Уроки</a>
                        </li>
                        <li className="header-menu__item"><a href="asd" className="header-menu__item__link">Стримы</a>
                        </li>
                    </ul>
                    <div className="header-profile">
                        <Link to="/login" className="header-profile__link">
                            Войти
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}


