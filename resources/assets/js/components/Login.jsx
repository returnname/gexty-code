import React, {Component} from 'react';



export default class Login extends Component {
    constructor(){
        super();


        this.state = {
            login: null,
            password:null
        };
    }
    handleInput = (e) => {
        const { name  , value } = e.target;
        this.setState({[name] : value});
    }

    handleClickButton = (e) =>{
        e.preventDefault();

        const { authActions } = this.props;
        const { login , password } = this.state;

        authActions.login({ login , password });
    }

    render() {
        const { handleInput , handleClickButton } = this;

        return (
            <div className="login">
                <form className="login-form">
                    <div className="login-form__container">
                        <label className="login-form__label">
                            Введите логин:
                        </label>
                        <input
                            type="text"
                            name="login"
                            className="login-form__input"
                            placeholder='Логин'
                            onInput={handleInput}
                        />
                        {/*asd*/}
                    </div>
                    <div className="login-form__container">
                        <label className="login-form__label">
                            Пароль
                        </label>
                        <input
                            type="password"
                            name="password"
                            className="login-form__input"
                            placeholder='Пароль'
                            onInput={handleInput}
                        />
                    </div>
                    <div className="login-form__container">
                        <button
                            className="login-form__button"
                            onClick={handleClickButton}
                        >Войти</button>
                    </div>
                </form>
            </div>
        );
    }
}


