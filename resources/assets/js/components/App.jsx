import React, {Component, Fragment} from 'react';
import Header from './Header'
import { Provider } from 'react-redux';
import { store  , browserHistory } from '../store/configureStore'
import { Router } from 'react-router'

import Content from './Content'

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={browserHistory}>
                    <div>
                        <Header/>
                        <Content/>
                    </div>
                </Router>
            </Provider>
        );
    }
}


