import React, {Component} from 'react';


class Post extends Component {
    componentDidMount(){
        const { id } = this.props.match.params;

        this.props.postActions.getPostItem(id);
    }

    componentWillUnmount(){
        this.props.postActions.postItemDisable();
    }

    render() {
        const { data , isLoaded } = this.props;

        if (!isLoaded) return <i className="fas fa-spinner"></i>;

        return (
            <div className="c-post">
                <h1 className="c-post__title">{data.title}</h1>
                <div className='c-post__thumb'>
                    <img src="/images//thumb.jpeg" className="c-post__thumb__image"/>
                </div>
                <p className="c-post__text">{data.text}</p>
            </div>
        );
    }
}
export default Post;


