import React, {Component , Fragment} from 'react';
import axios from 'axios';

export default class Greet extends Component {
    constructor(props){
        super(props);

        const { active , count } = props;

        this.state = {
            active: active ? active : false,
            count : count ? count : 0,
        }
    }
    render() {
        const { active , count } = this.state;

        const iconClassName = (active ? 'fas' : ' far') + ' fa-heart';

        return (
            <div className="greet">
                <button className="greet__button" onClick={this.handleClickGreet.bind(this)}>
                    <i className={iconClassName}></i>
                </button>
                <span className="greet__counter">{count}</span>
            </div>
        );
    }
    handleClickGreet(){

        this.setState(prev=> (
            {
                active : !prev.active ,
                count : prev.active ? --prev.count : ++prev.count
            })
        )
    }
}


