import { store } from './store/configureStore';

import jwtTokenService from "./services/jwtTokenService";
import httpService from "./httpService";


export default class AuthService {
    tokenService = jwtTokenService;

    authPath = '/api/auth/login';

    login(credetionals){
        const {  authPath , tokenService } = this;

        httpService.post( authPath , credetionals)
            .then(res=>{
                console.log(res.data);
                tokenService.setToken(res.data)
            })
            .catch(err=> console.log(err.response));
    }

    signOut(){

    }

    refreshToken(){
        console.log('asd')
        const { tokenService } = this;
        // try {
        //     let token =   await httpService.post('/api/auth/refresh')
        //     tokenService.setToken(token.data);
        // }catch (error) {
        //     console.log(err.response)
        // }
    }

}