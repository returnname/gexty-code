import thunk from "redux-thunk"
import { createLogger } from 'redux-logger'
import createHistory from 'history/createBrowserHistory'
import { createStore, applyMiddleware , combineReducers} from 'redux'
import createSagaMiddleware from 'redux-saga'

import postsReducer from './reducers/postsReducer'
import authReducer from './reducers/authReducer'

import authSaga from './sagas/auth'

import { routerMiddleware, routerReducer } from 'react-router-redux';

export const browserHistory = createHistory();

const logger = createLogger({});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


//!!LEGACY
// import jwtTokenService from "../services/jwtTokenService";
// import httpService from "../httpService";
// const jwtMiddleware = ({dispatch , getState}) => (next) => async (action) =>{
//     if (typeof action == 'function'){
//         if (jwtTokenService.hasToken() && jwtTokenService.tokenIsExpired()) {
//             try {
//                 let response = await httpService.get('/api/auth/refresh');
//                 dispatch({ type: 'REFRESH_TOKEN ' , tokenData: response.data })
//                 return next(action);
//             }catch (e) {
//                 dispatch({ type: 'REFRESH_TOKEN_FAILED' , error: e.response })
//                 return next(action);
//             }
//         }
//     }
//     return next(action);
// };

const sagaMiddleware = createSagaMiddleware()


export const configureStore = () => {
    const store = createStore (
        combineReducers({
            router: routerReducer,
            posts: postsReducer,
            auth: authReducer
        }),
        composeEnhancers(applyMiddleware(thunk, routerMiddleware(browserHistory), logger , sagaMiddleware))
    );

    sagaMiddleware.run(authSaga)

    return store;
};

export const store = configureStore();
