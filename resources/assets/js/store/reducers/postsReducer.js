import { postActionsTypes  as actions } from '../constants';
const initialState = {
    list: [],
    page: null,
    lastPage: null,
    isFetchingData: false,
    isHasError: false,
    postItem: {
        data: null,
        isLoaded: false,
        hasError:false
    }
};

const postsReducer = (state = initialState, action) => {
    switch (action.type){
        case actions.FETCH_POSTS:
            return { ...state, isFetchingData: true };
        case actions.FETCH_POSTS_SUCCESS:
            return {
                ...state,
                isFetchingData: false,
                list: state.list.concat(action.data),
                page: ++state.page,
                lastPage: action.meta.last_page,
            };
        case actions.FETCH_POSTS_FAILURE:
            return { ...state, isFetchingData: false, isHasError: true };
        case actions.FETCH_POST_ITEM:
            return { ...state, postItem : { ...state.postItem, isLoaded: false } };
        case actions.FETCH_POST_ITEM_SUCCESS:
            return { ...state, postItem : { ...state.postItem, isLoaded: true , data: action.post }};
        case actions.FETCH_POST_ITEM_FAILURE:
            return { ...state, postItem : { ...state.postItem, isLoaded: true , hasError: true }};
        case actions.POST_ITEM_EXIT:
            return { ...state, postItem : initialState.postItem };
        default:
            return state;
    }
};
export default postsReducer;
