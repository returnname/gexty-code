import jwtTokenService from "../../services/jwtTokenService";
import {
    LOGIN_REQUEST,
    LOGIN_REQUEST_SUCCESS,
    LOGIN_REQUEST_FAILED,
    REFRESH_TOKEN,
    REFRESH_TOKEN_FAILED
} from '../constants/auth'

const initialState = {
    isAuth : false,
    isLoaded: false
};

const authReducer = (state = initialState, action) => {
    switch (action.type){
        case LOGIN_REQUEST:
            return { ...state , isLoaded: true};
        case LOGIN_REQUEST_SUCCESS:
            jwtTokenService.setToken(action.tokenData);
            return { ...state , isAuth: true , isLoaded: false };
        case LOGIN_REQUEST_FAILED:
            jwtTokenService.destroyToken();
            return { ...state , isAuth: false , isLoaded: false };
        case REFRESH_TOKEN:
            jwtTokenService.setToken(action.tokenData);
            return { ...state , isAuth: true , isLoaded: false };
        case REFRESH_TOKEN_FAILED:
            //jwtTokenService.destroyToken();
            return { ...state , isAuth: false , isLoaded: false };
        default:
            return state;
    }
};
export default authReducer;
