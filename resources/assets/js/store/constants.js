export const postActionsTypes = {
    FETCH_POSTS: 'FETCH_POSTS',
    FETCH_POSTS_SUCCESS: 'FETCH_POSTS_SUCCESS',
    FETCH_POSTS_FAILURE: 'FETCH_POSTS_FAILURE',

    FETCH_POST_ITEM: 'FETCH_POST_ITEM',
    FETCH_POST_ITEM_SUCCESS: 'FETCH_POST_ITEM_SUCCESS',
    FETCH_POST_ITEM_FAILURE: 'FETCH_POST_ITEM_FAILURE',
    POST_ITEM_EXIT: 'POST_ITEM_EXIT'
};

