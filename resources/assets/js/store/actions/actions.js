import { postActionsTypes } from  '../constants'
import  httpService  from '../../httpService';

const postsActions = {
    fetchPostsFromApi: (page) => async (dispatch) => {
        dispatch({ type: postActionsTypes.FETCH_POSTS });
        try {
            let posts = await httpService.get('/api/post' , { params : { page }});
            dispatch({ type: postActionsTypes.FETCH_POSTS_SUCCESS , data : posts.data.data , meta: posts.data.meta });
        }catch (e) {
            dispatch({ type: postActionsTypes.FETCH_POSTS_FAILURE });
        }
    },
    getPostItem: (id) => async (dispatch , getState) =>{
        if (!id) return  dispatch({type: postActionsTypes.FETCH_POST_ITEM_FAILURE });
        const { list } = getState().posts;

        dispatch({ type: postActionsTypes.FETCH_POST_ITEM });

        let post = list.find(post => post.id == id);

        if (post)
            return dispatch({
                type: postActionsTypes.FETCH_POST_ITEM_SUCCESS,
                post
            });

        try {
            post = await httpService.get(`/api/post/${id}`);
            dispatch({type: postActionsTypes.FETCH_POST_ITEM_SUCCESS, post: post.data.data});
        }catch (e) {
            dispatch({type: postActionsTypes.FETCH_POST_ITEM_FAILURE });
        }
    },
    postItemDisable: () => (dispatch)=> {
        dispatch({ type: postActionsTypes.POST_ITEM_EXIT });
    }
};
export default postsActions;