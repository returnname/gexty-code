import httpService from "../../httpService";

import {
    LOGIN_REQUEST,
    LOGIN_REQUEST_SUCCESS,
    LOGIN_REQUEST_FAILED
} from '../constants/auth'

const authActions = {
    login:  ({ login , password }) => async (dispatch) => {
        dispatch({ type: LOGIN_REQUEST });

        try {
            let response = await httpService.post('/api/auth/login' , { login , password });
            dispatch({ type: LOGIN_REQUEST_SUCCESS , tokenData : response.data});
        }catch (e) {
            dispatch({ type: LOGIN_REQUEST_FAILED });
        }
    }
};
export default authActions;