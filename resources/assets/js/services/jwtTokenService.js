import moment from 'moment';


class JWTTokenService {
    tokenAlias = 'access_token';

    setToken( {access_token , expires_in } ){
        const { tokenAlias } = this;

        localStorage.setItem(tokenAlias , access_token);
        localStorage.setItem('expires_in' , expires_in);

        //localStorage.setItem('token_type' , token_type);
    }

    destroyToken(){
        const { tokenAlias } = this;
        localStorage.removeItem(tokenAlias);
        localStorage.removeItem('expires_in');
    }

    hasToken(){
        const { tokenAlias } = this;
        return localStorage.getItem(tokenAlias) !== null;
    }

    getToken(){
        const { tokenAlias } = this;
        return localStorage.getItem(tokenAlias);
    }

    tokenIsExpired(){
        let expiredDate = moment(localStorage.getItem('expires_in'));

        return expiredDate.isBefore(moment());
    }

}

export default new JWTTokenService();