import {render} from "react-dom";
import React from 'react';
import App from './Components/App';


if (document.getElementById('app')) {
    render(<App />, document.getElementById('app'));
}