import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import postActions from "../store/actions/actions";
import Post from '../components/Post/Post'

const mapStateToProps = (state) => (
    {
       data: state.posts.postItem.data,
       isLoaded: state.posts.postItem.isLoaded
    }
);

const mapDispatchToProps = dispatch => ({ postActions: bindActionCreators(postActions, dispatch)});

export default connect(mapStateToProps, mapDispatchToProps)(Post);