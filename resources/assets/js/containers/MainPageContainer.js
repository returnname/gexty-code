import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import postActions from "../store/actions/actions";
import FeedList from '../components/Feed/FeedList'

const mapStateToProps = (state) => (
    {
        posts : state.posts.list,
        page: state.posts.page,
        lastPage : state.posts.lastPage,
        isFetchingData : state.posts.isFetchingData,
    }
);

const mapDispatchToProps = dispatch => ({postActions: bindActionCreators(postActions, dispatch)});

export default connect(mapStateToProps, mapDispatchToProps)(FeedList);