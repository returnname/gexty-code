import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Login from "../components/Login";

import authActions from "../store/actions/auth";

const mapStateToProps = (state) => (
    {
        isAuth: state.auth.isAuth,
        isLoaded: state.auth.isLoaded
    }
);
const mapDispatchToProps = dispatch => ({ authActions: bindActionCreators(authActions, dispatch)});

export default connect(mapStateToProps, mapDispatchToProps)(Login);