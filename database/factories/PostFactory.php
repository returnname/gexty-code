<?php

use Faker\Generator as Faker;


$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(8),
        'text' => $faker->text(1500),
        'user_id' => 1,
    ];
});
